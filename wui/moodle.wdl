<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:model sc:refUri="/extensions/moodle/model/mdlExp.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:headBodyWidget>
				<sm:container>
					<sm:cssRules xml:space="preserve">.head{
    margin: 1.5em .1em .5em .1em;
    padding: .2em .7em;
    background-color: var(--inv-alt2-bgcolor);
    color: var(--inv-alt2-color);
    font-weight: bold;
    border-top-left-radius: .7em;
    border-top-right-radius: .7em;
    border-radius: .7em;
    align-self: flex-start;
}
.label {
min-width: initial;
}</sm:cssRules>
				</sm:container>
			</sm:headBodyWidget>
		</sm:root>
	</sm:editPoints>
</sm:compositionBoxWdl>
