<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:op="utc.fr:ics/opale3" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="sc sp op java">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:variable name="vRandom" select="java:java.util.Random.new()"/>
	<xsl:variable name="gap" select="getDialogVar('gap')"/>
	<xsl:template match="op:gapM">
		<xsl:choose>
			<xsl:when test="sp:options">
      {1:MULTICHOICE:<xsl:apply-templates select="sp:options"/>}
    </xsl:when>
			<xsl:otherwise>
      {1:SHORTANSWER:=<xsl:value-of select="$gap"/>
				<xsl:apply-templates select="sp:synonyms"/>}
    </xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="sp:options">
		<xsl:for-each select="sp:option">
			<xsl:sort select="java:nextDouble($vRandom)" data-type="number"/>
			<xsl:variable name="option" select="."/>
			<xsl:if test="position() != 1">~</xsl:if>
			<xsl:if test="$option = $gap or count(//sp:synonym[text() = $option ]) &gt;0">%100%</xsl:if>
			<xsl:value-of select="$option"/>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="sp:synonyms">
		<xsl:for-each select="sp:synonym">
			<xsl:if test="./text() != $gap">~%100%<xsl:value-of select="."/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="*"/>
</xsl:stylesheet>