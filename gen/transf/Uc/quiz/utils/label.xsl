<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
  xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
  xmlns:op="utc.fr:ics/opale3"
  exclude-result-prefixes="sc sp op">
<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
<xsl:param name="vDialog"/>
<xsl:param name="vAgent"/>

<xsl:variable name="axis" select="getDialogVar('axis')"/>

<xsl:template match="op:label">
    <xsl:apply-templates select="sp:*" />
</xsl:template>

<xsl:template match="sp:txt | sp:img">
  <xsl:choose>
    <xsl:when test="$axis = 'label'"><xsl:variable name="label" select="getContent(gotoSubModel(),'label')" /></xsl:when>
    <xsl:when test="$axis = 'target'"><xsl:variable name="target" select="getContent(gotoSubModel(),'target')" /></xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template match="*"/>

</xsl:stylesheet>
