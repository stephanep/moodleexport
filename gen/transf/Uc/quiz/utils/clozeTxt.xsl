<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
  xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
  xmlns:op="utc.fr:ics/opale3"
  exclude-result-prefixes="sc sp op">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
  <xsl:param name="vDialog"/>
  <xsl:param name="vAgent"/>
  <xsl:include href="intern:bswsp:~param/libMoodle/txt.xsl" />

  <xsl:template match="op:clozeTxt">
    <xsl:apply-templates select="sc:*"/>
  </xsl:template>

  <!-- définition d'un paragraphe avec un line-height égale à 2 (sans unité) -->
  <xsl:template match="sc:para">
    <p style="line-height: 2"><xsl:apply-templates select="sc:*|text()"/></p>
  </xsl:template>

  <xsl:template match="sc:textLeaf[@role='gap']">
    <xsl:choose>
      <xsl:when test="op:gapM">
        <xsl:variable name="gap" select="setDialogVar('gap', ./child::text())" />
        <xsl:value-of select="getContent(gotoMeta(),'')" disable-output-escaping="yes"/>
      </xsl:when>
      <xsl:otherwise>{1:SHORTANSWER:=<xsl:value-of select="."/>}</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="*"/>

</xsl:stylesheet>
