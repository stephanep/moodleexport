<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
  xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
  xmlns:op="utc.fr:ics/opale3"
  exclude-result-prefixes="sc sp op">
<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
<xsl:param name="vDialog"/>
<xsl:param name="vAgent"/>

  <xsl:template match="op:labelImg">
    <xsl:apply-templates select="sp:img"/>
  </xsl:template>

  <xsl:template match="sp:img">
    <xsl:call-template name="image" />
  </xsl:template>

  <xsl:template name="image">
    <xsl:variable name="title"><xsl:value-of select="normalize-space(getContent(gotoSubModel(),'label'))" disable-output-escaping="yes" /></xsl:variable>
    <xsl:variable name="new">
      <xsl:copy-of select="getDialogVar('matchCo')"/>
      <xsl:choose>
        <xsl:when test="string-length($title) > 0"><target type="text" title="{$title}"/></xsl:when>
        <xsl:otherwise><target type="text" title="Image sans titre"/></xsl:otherwise>
      </xsl:choose>

    </xsl:variable>
    <xsl:variable name="matchCo" select="setDialogVar('matchCo',$new)"/>
  </xsl:template>

  <xsl:template match="*"/>

</xsl:stylesheet>