<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
  xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
  xmlns:op="utc.fr:ics/opale3"
  xmlns:java="http://xml.apache.org/xslt/java"
  exclude-result-prefixes="sc sp op java">
<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
<xsl:param name="vDialog"/>
<xsl:param name="vAgent"/>
<xsl:include href="intern:bswsp:~param/libMoodle/txt@text.xsl" />
<xsl:variable name="vRandom" select="java:java.util.Random.new()"/>

  <xsl:template match="op:labelTxt">
    <xsl:variable name="txt"><xsl:apply-templates select="sc:*"/></xsl:variable>
    <xsl:variable name="new">
      <xsl:copy-of select="getDialogVar('matchCo')"/>
      <label type="text" title="{$txt}" random="{java:nextDouble($vRandom)}"/>
    </xsl:variable>
    <xsl:variable name="matchCo" select="setDialogVar('matchCo',$new)"/>
  </xsl:template>

  <xsl:template match="*"/>

</xsl:stylesheet>