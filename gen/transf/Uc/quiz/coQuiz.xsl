<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:op="utc.fr:ics/opale3" exclude-result-prefixes="sc sp op">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:include href="intern:bswsp:~param/libMoodle/categoryName.xsl"/>
	<xsl:variable name="QuizNameInTitle" select="getDialogVar('QuizNameInTitle')"/>
	<xsl:template match="op:coQuiz">
		<xsl:if test="$organizing = 'true' and string-length($localName) &gt; 0">
			<question type="category">
				<category>
					<text>
						<xsl:if test="$QuizNameInTitle = 'yes'">
							<xsl:value-of select="substring-before(extractFileNameFromPath(srcFields(srcFileAgent(), 'srcUri')), '.quiz')"/>
							<xsl:text> </xsl:text>
						</xsl:if>
						<xsl:value-of select="$fullName" disable-output-escaping="yes"/>
					</text>
				</category>
			</question>
		</xsl:if>
		<xsl:apply-templates select="sp:trainUcMcqSur | sp:trainUcMcqMur | sp:trainUcCloze | sp:trainUcField | sp:trainUcNumerical"/>
	</xsl:template>
	<xsl:template match="sp:trainUcMcqSur | sp:trainUcMcqMur | sp:trainUcCloze | sp:trainUcField | sp:trainUcNumerical">
		<xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
	</xsl:template>
	<xsl:template match="*"/>
</xsl:stylesheet>