<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  
	xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" 
	xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
	xmlns:op="utc.fr:ics/opale3" 
	exclude-result-prefixes="sc sp op">
<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
<xsl:param name="vDialog"/>
<xsl:param name="vAgent"/>

<xsl:variable name="organizing" select="getDialogVar('organizing')"/>

<xsl:template match="op:assmntUa">		
	<xsl:if test="$organizing = 'true' and sp:quiz">
		<!-- si on crée automatiquement une arborescence de catégories alors on calcule le nom complet -->
		<xsl:variable name="fullName_old" select="getDialogVar('fullName')"/>
		<xsl:variable name="localName" select="getContent(gotoMeta(),'')" />
		<xsl:variable name="fullName">
			<xsl:if test="string-length($fullName_old) = 0"><xsl:value-of select="$localName" /></xsl:if>
			<xsl:if test="string-length($fullName_old) > 0"><xsl:value-of select="concat($fullName_old,'/',$localName)" /></xsl:if>
		</xsl:variable>
		<!-- et on crée la catégorie -->
		<question type="category">
    	<category>
				<text><xsl:value-of select="$fullName" disable-output-escaping="yes"/></text>
    	</category>
  	</question>
	</xsl:if>
	<xsl:apply-templates select="sp:quiz" />
</xsl:template>	

<xsl:template match="sp:quiz">
	<xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
</xsl:template>

<xsl:template match="*"/>

</xsl:stylesheet>
