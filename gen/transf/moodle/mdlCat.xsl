<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
  xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
  xmlns:op="utc.fr:ics/opale3"
  exclude-result-prefixes="sc sp op">
<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
<xsl:param name="vDialog"/>
<xsl:param name="vAgent"/>
<xsl:include href="intern:bswsp:~param/libMoodle/categoryName.xsl" />

<xsl:variable name="organizing" select="getDialogVar('organizing')"/>
  
<xsl:template match="op:mdlCat">
  <question type="category">
    <category>
      <text>
        <xsl:value-of select="$fullName" disable-output-escaping="yes"/><!--<xsl:value-of select="getContent(gotoMeta(),'')" disable-output-escaping="yes"/>-->
      </text>
    </category>
  </question>

  <xsl:choose>
    <xsl:when test="$organizing = 'true'">
      <xsl:apply-templates select="sp:trainUcMcqSur | sp:trainUcMcqMur | sp:trainUcMatch | sp:trainUcOrdWord | sp:trainUcCloze | sp:trainUcField | sp:trainUcNumerical" />
      <xsl:apply-templates select="sp:trainUcCoQuiz | sp:ue | sp:assmntUa  | sp:courseUa | sp:cat" />
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select="sp:trainUcMcqSur | sp:trainUcMcqMur | sp:trainUcMatch | sp:trainUcOrdWord | sp:trainUcCloze | sp:trainUcField | sp:trainUcNumerical | sp:trainUcCoQuiz | sp:ue | sp:assmntUa | sp:courseUa" />
      <xsl:apply-templates select="sp:cat" />
    </xsl:otherwise>
  </xsl:choose>

</xsl:template>

<xsl:template match="sp:trainUcMcqSur | sp:trainUcMcqMur | sp:trainUcMatch | sp:trainUcCloze | sp:trainUcField | sp:trainUcNumerical">
  <xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
</xsl:template>

<xsl:template match="sp:trainUcOrdWord">
  <xsl:variable name="vPos" select="setDialogVar('vPos', position())"/> 
  <xsl:variable name="globalType" select="getDialogVar('ordWordTypeGlobal')" />
  <xsl:variable name="localType" select="normalize-space(getContent(gotoMeta(),''))" />
  <xsl:if test="string-length($localType) = 0"><xsl:variable name="type" select="setDialogVar('ordWordType',$globalType)"/></xsl:if>
  <xsl:if test="string-length($localType) > 0"><xsl:variable name="type" select="setDialogVar('ordWordType',$localType)"/></xsl:if>
  <xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
</xsl:template>

<xsl:template match="sp:cat">
  <xsl:variable name="fullName_cat" select="setDialogVar('fullName',$fullName)"/>
  <xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
</xsl:template>

<xsl:template match="sp:ue | sp:assmntUa | sp:courseUa | sp:trainUcCoQuiz">
  <xsl:if test="$organizing = 'true'">
    <xsl:variable name="fullName_ue" select="setDialogVar('fullName',$fullName)"/>
  </xsl:if>
  <xsl:variable name="type" select="setDialogVar('ordWordType',getDialogVar('ordWordTypeGlobal'))"/>
  <xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
</xsl:template>

<xsl:template match="*"/>

</xsl:stylesheet>
