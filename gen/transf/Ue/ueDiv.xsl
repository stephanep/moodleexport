<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  
	xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" 
	xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
	xmlns:op="utc.fr:ics/opale3" 
	exclude-result-prefixes="sc sp op">
<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
<xsl:param name="vDialog"/>
<xsl:param name="vAgent"/>

<xsl:variable name="organizing" select="getDialogVar('organizing')"/>
<xsl:variable name="fullName_old" select="getDialogVar('fullName')"/>
<xsl:variable name="localName" select="getContent(gotoMeta(),'')" />
<xsl:variable name="fullName">
	<xsl:if test="string-length($fullName_old) = 0"><xsl:value-of select="$localName" /></xsl:if>
	<xsl:if test="string-length($fullName_old) > 0"><xsl:value-of select="concat($fullName_old,'/',$localName)" /></xsl:if>
</xsl:variable>

<xsl:template match="op:ueDiv">		
	<xsl:choose>
		<xsl:when test="$organizing = 'true'">
			<!-- s'il y a des quiz, on crée la catégorie -->
			<xsl:if test="sp:trainUcMcqSur or sp:trainUcMcqMur or sp:trainUcMatch or sp:trainUcOrdWord or sp:trainUcCloze or sp:trainUcField">
				<question type="category">
		    	<category>
						<text><xsl:value-of select="$fullName" disable-output-escaping="yes"/></text>
		    	</category>
		  	</question>
			</xsl:if>
			<!-- on commence par traiter les quiz puis le reste -->
			<xsl:apply-templates select="sp:trainUcMcqSur | sp:trainUcMcqMur | sp:trainUcMatch | sp:trainUcOrdWord | sp:trainUcCloze | sp:trainUcField" />
			<xsl:apply-templates select="sp:div | sp:assmntUa | sp:courseUa" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates select="sp:*" />
		</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>	

<xsl:template match="sp:trainUcMcqSur | sp:trainUcMcqMur | sp:trainUcMatch | sp:trainUcOrdWord | sp:trainUcCloze | sp:trainUcField">
	<xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
</xsl:template>

<xsl:template match="sp:div | sp:assmntUa | sp:courseUa">
	<xsl:if test="$organizing = 'true'">
		<xsl:variable name="fullName_new" select="setDialogVar('fullName',$fullName)"/>
	</xsl:if>
	<xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
</xsl:template>

<xsl:template match="*"/>

</xsl:stylesheet>
