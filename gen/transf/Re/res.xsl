<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:op="utc.fr:ics/opale3" exclude-result-prefixes="sc sp op">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:include href="intern:bswsp:~param/libMoodle/img.xsl"/>
	<xsl:include href="intern:bswsp:~param/libMoodle/mathtex.xsl"/>
	<xsl:template match="op:res">
		<xsl:apply-templates select="sp:*"/>
	</xsl:template>
	<xsl:template match="sp:txt | sp:txtRes">
		<xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
	</xsl:template>
	<xsl:template match="sp:res">
		<xsl:variable name="uri" select="srcFields(srcFileAgent(concat('@', getIdFromPath(@sc:refUri))), 'itModel')"/>
		<xsl:variable name="loweruri" select="translate($uri,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')"/>
		<xsl:choose>
			<xsl:when test="$uri='sfile_odf' or $uri='sfile_ods' or $uri='sfile_odg' or $uri='sfile_png_gif_jpg_jpeg' or $uri='sfile_svg'">
				<xsl:call-template name="image"/>
			</xsl:when>
			<xsl:when test="contains($loweruri,'.mtex')">
				<!--
  		-->
				<xsl:call-template name="mathtex">
					<xsl:with-param name="equation" select="getContent(gotoSubModel(),'')"/>
				</xsl:call-template>
				<!--
  		-->
			</xsl:when>
			<xsl:otherwise>
				<!-- les cas non traités -->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="sp:listing">
		<xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
	</xsl:template>
	<xsl:template match="sp:bkquote">
		<div style="margin: 1em 5em; position: relative;">
			<span style="font-size: 3em; line-height: 0.5; position: absolute; top: 0; left: 0;">«</span>
			<xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
			<span style="font-size: 3em; line-height: 0.5; position: absolute; bottom: 0; right: 0;">»</span>
		</div>
	</xsl:template>
	<xsl:template match="sp:filtered">
		<xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes"/>
	</xsl:template>
</xsl:stylesheet>