<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
  xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
  xmlns:op="utc.fr:ics/opale3"
  exclude-result-prefixes="sc sp op">
  <xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
  <xsl:param name="vDialog"/>
  <xsl:param name="vAgent"/>
  <xsl:include href="intern:bswsp:~param/libMoodle/mathtex.xsl" />

  <xsl:template match="sc:*">
    <xsl:apply-templates select="sc:*|text()"/>
  </xsl:template>

  <xsl:template match="text()">
    <xsl:copy-of select="."/>
  </xsl:template>

  <xsl:template match="sc:textLeaf[@role='mathtex']">
    <xsl:call-template name="mathtex">
      <xsl:with-param name="equation" select="." />
      <xsl:with-param name="display" select="'inline'" />
    </xsl:call-template>
  </xsl:template>
  <xsl:template match="sc:inlineImg[@role='form']">
    <xsl:if test="contains(@sc:refUri,'.mtex')">
      <xsl:call-template name="mathtex">
        <xsl:with-param name="equation" select="getContent(gotoSubModel(),'')" />
        <xsl:with-param name="display" select="'inline'" />
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template match="*"/>

</xsl:stylesheet>