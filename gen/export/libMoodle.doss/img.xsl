<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
  xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
  xmlns:op="utc.fr:ics/opale3"
  exclude-result-prefixes="sc sp op">

<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
<xsl:param name="vDialog"/>
<xsl:param name="vAgent"/>
<xsl:variable name="quote">"</xsl:variable>

<xsl:template name="image">
  <xsl:param name="axis" select="string('')"/>
  <xsl:param name="pos" select="string('lft')"/>
  <xsl:variable name="tag">
    <xsl:choose>
      <xsl:when test="$axis = 'ico'"><xsl:value-of select="getContent(gotoSubModel(),'ico')" disable-output-escaping="yes" /></xsl:when>
      <xsl:when test="$axis = 'txtRes'"><xsl:value-of select="getContent(gotoSubModel(),'txtRes')" disable-output-escaping="yes" /></xsl:when>
      <xsl:otherwise><xsl:value-of select="getContent(gotoSubModel(),'')" disable-output-escaping="yes" /></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="src" select="substring-before(substring-after($tag,'res/'),$quote)" />
  <xsl:variable name="width" select="substring-before(substring-after($tag,concat('width=',$quote)),$quote)" />
  <xsl:variable name="height" select="substring-before(substring-after($tag,concat('height=',$quote)),$quote)" />

  <!-- création de la balise <img> dans le CDATA -->
  <xsl:variable name="imgTag">
    <xsl:element name="img">
      <xsl:attribute name="src">
        <xsl:value-of select="concat('@@PLUGINFILE@@/',$src)" />
      </xsl:attribute>
      <xsl:attribute name="width">
        <xsl:value-of select="$width" />
      </xsl:attribute>
      <xsl:attribute name="height">
        <xsl:value-of select="$height" />
      </xsl:attribute>
      <xsl:if test="$axis = 'ico'">
        <xsl:attribute name="style">vertical-align: <xsl:value-of select="-floor(number($height) div 2)" />px;</xsl:attribute>
      </xsl:if>
      <xsl:if test="$axis = 'txtRes'">
        <xsl:if test="$pos = 'lft'">
          <xsl:attribute name="style">float: left; margin: 0 10px 10px 0;</xsl:attribute>
        </xsl:if>
        <xsl:if test="$pos = 'rgt'">
          <xsl:attribute name="style">float: right; margin: 0 0 10px 10px;</xsl:attribute>
        </xsl:if>
      </xsl:if>
    </xsl:element>
  </xsl:variable>

  <xsl:choose>
    <xsl:when test="$axis = 'ico' or $axis = 'txtRes'">
      <xsl:copy-of select="$imgTag" />
    </xsl:when>
    <xsl:otherwise>
      <div style="text-align: center;">
        <xsl:copy-of select="$imgTag" />
        <xsl:variable name="title" select="normalize-space(getContent(gotoSubModel(),'title'))"></xsl:variable>
        <xsl:if test="string-length($title) > 0">
          <p style="font-style: italic; color: gray;"><xsl:value-of select="$title" disable-output-escaping="yes" /></p>
        </xsl:if>
      </div>
    </xsl:otherwise>
  </xsl:choose>


  <!-- ajout de l'élément dans la variable file qui contient les balises <file> -->
  <xsl:variable name="files" select="getDialogVar('files')" />
  <xsl:if test="not(contains($files, concat($quote,$src,$quote)))">
    <xsl:variable name="file">&lt;file name="<xsl:value-of select="$src" />" encoding="base64"&gt;&lt;/file&gt;</xsl:variable>
    <xsl:variable name="newfiles" select="setDialogVar('files', concat($files,$file) )"/>
  </xsl:if>

</xsl:template>

</xsl:stylesheet>
