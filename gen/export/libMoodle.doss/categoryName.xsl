<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
  xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
  xmlns:op="utc.fr:ics/opale3"
  xmlns:opa="kelis.fr:opa"
  exclude-result-prefixes="sc sp op opa">

<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
<xsl:param name="vDialog"/>
<xsl:param name="vAgent"/>

<xsl:variable name="fullName_old" select="getDialogVar('fullName')"/>
<xsl:variable name="name" select="normalize-space(getContent(gotoMeta(),''))" />
<xsl:variable name="localName">
  <xsl:call-template name="rechercher-remplacer">
    <xsl:with-param name="chaine" select="$name"/>
    <xsl:with-param name="cherche" select="'/'"/>
    <xsl:with-param name="remplace" select="'//'"/>
  </xsl:call-template>
</xsl:variable>
<xsl:variable name="fullName">
  <xsl:if test="string-length($fullName_old) = 0"><xsl:value-of select="$localName" /></xsl:if>
  <xsl:if test="string-length($fullName_old) > 0"><xsl:value-of select="concat($fullName_old,'/',$localName)" /></xsl:if>
</xsl:variable>


<!-- règle de chercher-remplacer -->
<xsl:template name="rechercher-remplacer">
  <!-- 3 paramètres : chaîne à traiter, le motif à chercher, et celui qui le remplacera -->
  <xsl:param name="chaine"/>
  <xsl:param name="cherche"/>
  <xsl:param name="remplace"/>
  <xsl:choose>
    <!-- si on a bien défini cherche et remplace et si la chaine contient bien le motif cherche, on copie ce qu'il y a avant puis le motif remplace et exécute récursivement cette règle sur la fin de chaine -->
    <xsl:when test="$cherche and $remplace and contains ($chaine,$cherche)">
      <xsl:value-of select="substring-before($chaine,$cherche)"/>
      <xsl:value-of select="$remplace"/>
      <xsl:call-template name="rechercher-remplacer">
        <xsl:with-param name="chaine" select="substring-after($chaine,$cherche)"/>
        <xsl:with-param name="cherche" select="$cherche"/>
        <xsl:with-param name="remplace" select="$remplace"/>
      </xsl:call-template>
    </xsl:when>
    <!-- sinon on se contente de copier le contenu de la chaine -->
    <xsl:otherwise>
      <xsl:value-of select="$chaine"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="substring-after-last">
<xsl:param name="input"/>
<xsl:param name="substr"/>
<!-- on récupère la chaîne après la première occurrence -->
<xsl:variable name="temp" select="substring-after($input,$substr)"/>
<xsl:choose>
  <!-- si celle-ci contient une occurrence, on recommence  -->
  <xsl:when test="$substr and contains($temp,$substr)">
    <xsl:call-template name="substring-after-last">
      <xsl:with-param name="input" select="$temp"/>
      <xsl:with-param name="substr" select="$substr"/>
    </xsl:call-template>
  </xsl:when>
  <xsl:otherwise>
    <xsl:value-of select="$temp"/>
  </xsl:otherwise>
</xsl:choose>
</xsl:template>

</xsl:stylesheet>