<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
  xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
  xmlns:op="utc.fr:ics/opale3"
  exclude-result-prefixes="sc sp op">

<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
<xsl:param name="vDialog"/>
<xsl:param name="vAgent"/>

<xsl:variable name="latex" select="getDialogVar('latex')"/>

<xsl:template name="mathtex">
  <xsl:param name="equation" />
  <xsl:param name="display" select="''" />
  <xsl:variable name="clean_equation" select="translate($equation,'&#xa0;&#8239;','  ')" />
  <xsl:choose>
    <xsl:when test="$display = 'inline' and $latex = 'mimetex'">$$\textstyle{<xsl:value-of select="$clean_equation" />}$$</xsl:when>
    <xsl:when test="$display = 'inline' and $latex = 'mathjax'">\(<xsl:value-of select="$clean_equation" />\)</xsl:when>
    <xsl:when test="$display = '' and $latex = 'mimetex'">$$<xsl:value-of select="$clean_equation" />$$</xsl:when>
    <xsl:when test="$display = '' and $latex = 'mathjax'">\[<xsl:value-of select="$clean_equation" />\]</xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
