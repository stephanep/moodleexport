<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="no"/>
<xsl:param name="path"/>

	<!-- insertion des encodages base64 -->
	<xsl:template match="file">
		<xsl:variable name="name" select="@name" />
		<xsl:variable name="xml" select="concat($path,'res/',$name,'.xml')" />
		<xsl:variable name="base64" select="string(document($xml)/base64)" />
		<file name="{$name}" encoding="base64"><xsl:value-of select="$base64" /></file>
	</xsl:template>
	
	<!-- ajout des CDATA aux bons endroits -->
	<xsl:template match="text[./parent::*/@format = 'html'] | text[./parent::questiontext and ./ancestor::question/@type = 'cloze']">
		<xsl:copy>
			<xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
			<xsl:apply-templates select="@*|node()"/>
			<xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
		</xsl:copy>
	</xsl:template>
	
	<!-- suppression des balises p dans les listings -->
	<xsl:template match="//pre/p"><xsl:apply-templates select="@*|node()"/></xsl:template>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
  </xsl:template>

</xsl:stylesheet>
